<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<title>Insert title here</title>
<style type="text/css">
form{padding: 20px; font-size: 11px}
.title{}
.title>input{width:50px;}
.target_url{width:400px;}
.result{width:500px;}
</style>
</head>
<body>
	<form method="post">
		
		<div class="title">	
			<input type="text" class="year" value="" placeholder="2014" />년 
			<input type="text" class="month" value="" placeholder="09" />월 뉴스레터
		</div>
		<br/>
		캠페인 매체 <input type="text" class="email" value="이메일" /> <br/>
		캠페인 이름 <input type="text" class="camp_name" value="" /> <br/>
		
		<br/>
		<br/>
		<table>
			<thead>
				<tr>
					<th>켐페인 소스</th>
					<th>웹 사이트 URL</th>
					<th>결과</th>
				</tr>
			</thead>
			<c:forEach begin="1" end="10">
				<tr>
					<td><input type="text" class="camp_source"></td>
					<td><input type="text" class="target_url"></td>
					<td class="result"></td>
				</tr>
			</c:forEach>
		</table>
		
		<button type="button" onclick="addInput()"> +</button><br/>
		<button type="button" onclick="generate()">생성</button>
	</form>

</body>
<script type="text/javascript">
	var addInput = function(){
		var html = '<tr>'+
							'<td><input type="text" class="camp_source"></td>'+
							'<td><input type="text" class="target_url"></td>'+
							'<td class="result"></td>'+
						'</tr>';
		
		$('table').append(html);
	};
	
	var generate = function(){
		var email =  encodeURI($('.email').val());
		var camp_name = encodeURI($('.camp_name').val());
		var size = $('.target_url').length;
		for(var idx = 0; idx < size; idx++ ){
			if($('.target_url:eq('+idx+')').val() != '' ) {
				$('.result:eq('+idx+')').text( $('target_url:eq('+idx+')').val()+'/?utm_source='+encodeURI($('.camp_source:eq('+idx+')').val())+'&utm_medium='+email+'&utm_campaign='+camp_name );
			}
		}
		
		for(var idx = 0, size = $('input').length; idx < size ; idx++ ){
			localStorage.setItem('');	
		}
		 
		
	};
</script>
</html>