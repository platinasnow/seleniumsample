package org.webdriver.selenium;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Event2Test {

	// @Test
	public void 프로야구_raq_member() throws InterruptedException {
		raq_ratd_member("http://localhost:8080/chevy/power-of-play/baseball-raq-member.gm");
	}

	// @Test
	public void 프로야구_raq_nomember() throws InterruptedException {
		raq_ratd_nomember("http://localhost:8080/chevy/power-of-play/baseball-raq-nomember.gm");
	}

	@Test
	public void 프로야구_ratd_nomember() throws InterruptedException {
		raq_ratd_nomember("http://localhost:8080/chevy/power-of-play/baseball-ratd-nomember.gm");
	}

	@Test
	public void 말리부_ratd_member() {
		raq_ratd_member("http://localhost:8080/campaign/my15-malibu/event2-ratd-member.gm");
	}

	@Test
	public void 슈퍼세이프티_ratd_nomember() {
		raq_ratd_member("http://localhost:8080/campaign/2014-super-safety/event2-ratd-member.gm");
	}


	public void raq_ratd_nomember(String url) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Snow\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		// 이벤트2
		driver.get(url);
		nomemberRegist(driver, executor);
	}

	public void raq_ratd_member(String url) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Snow\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		JavascriptExecutor executor = (JavascriptExecutor) driver;

		// 로그인
		login(driver, executor);
		// 회원 시승신청
		driver.navigate().to(url);
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#ymOfficeName")));
		// 공통 정보 입력부분
		event2CommonRegist(driver, executor);
	}

	public void login(WebDriver driver, JavascriptExecutor executor) {
		driver.get("http://localhost:8080/member/login.gm");
		driver.findElement(By.cssSelector("#id")).sendKeys("platinasnow");
		driver.findElement(By.cssSelector("#passwd")).sendKeys("Test1234");
		executor.executeScript("arguments[0].click();", driver.findElement(By.cssSelector("#btnLogin")));
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".visual_container")));
	}

	public void nomemberRegist(WebDriver driver, JavascriptExecutor executor) {
		// 비회원 응모
		driver.findElement(By.cssSelector("#name")).sendKeys("강아지");
		driver.findElement(By.cssSelector("#cphone2")).sendKeys("5555");
		driver.findElement(By.cssSelector("#cphone3")).sendKeys("1212");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#zip01")), "123");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#zip02")), "123");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector(".addTxt1")), "테스트 주소");
		driver.findElement(By.cssSelector(".addTxt2")).sendKeys("테스트 상세주소");
		driver.findElement(By.cssSelector(".email")).sendKeys("test");
		driver.findElement(By.cssSelector("#email2")).sendKeys("uber.co.kr");

		// 공통 정보 입력부분
		event2CommonRegist(driver, executor);
	}

	public void event2CommonRegist(WebDriver driver, JavascriptExecutor executor) {
		driver.findElement(By.cssSelector("#car1")).click();
		try {
			driver.findElement(By.cssSelector("#purchaseYear")).click();
			driver.findElement(By.cssSelector("#purchaseYear>option:nth-child(2)")).click();
		} catch (Exception e) {
		}

		executor.executeScript("arguments[0].click();", driver.findElement(By.cssSelector("#agreeAll")));
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#dlCd")), "KR05");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#ymOfficeCd")), "K5S090");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#ymCd")), "K512B388");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#ymOfficeName")), "마포중앙");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#ymName")), "주지유");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector("#isRnd")), "0");

		executor.executeScript("arguments[0].click();", driver.findElement(By.cssSelector("#btnReg")));
		driver.switchTo().alert().accept();
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#content")));
		driver.quit();
	}

}
