package org.webdriver.selenium;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriveTest {

	//@Test
	public void firefoxTest(){
		WebDriver driver = new FirefoxDriver();
		driver.get("http://localhost:5656/selenium/");
		
		driver.quit();
	}
	
	
	//@Test
	public void chromeTest(){
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Snow\\Downloads\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://seleniumhtml.appspot.com");
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		
		driver.findElement(By.cssSelector(".name")).sendKeys("�������");
		executor.executeScript("arguments[0].value=arguments[1]", driver.findElement(By.cssSelector(".address")), "Seoul");
		driver.findElement(By.cssSelector("input[value=apple]")).click();
		
		driver.findElement(By.cssSelector("select")).click();
		driver.findElement(By.cssSelector("select>option:nth-child(3)")).click();
		
		//executor.executeScript("arguments[0].click", driver.findElement(By.cssSelector(".confirm-btn")));
		driver.findElement(By.cssSelector(".confirm-btn")).click();
		driver.switchTo().alert().dismiss();
		//driver.switchTo().alert().accept();
		
		driver.findElement(By.cssSelector(".submit-btn")).submit();
	}
	
	
	//@Test
	public void testIEBrowser(){
		System.setProperty("webdriver.ie.driver", "C:\\Users\\Snow\\Downloads\\IEDriverServer.exe");
		WebDriver driver = new InternetExplorerDriver();
		driver.navigate().to("http://www.naver.com");
	}
	
	
	
	
	
	
	
	
}
